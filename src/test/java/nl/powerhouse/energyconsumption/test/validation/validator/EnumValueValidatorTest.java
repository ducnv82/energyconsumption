package nl.powerhouse.energyconsumption.test.validation.validator;

import nl.powerhouse.energyconsumption.test.validation.BaseValidatorTest;
import nl.powerhouse.energyconsumption.validation.constraints.EnumValue;
import org.junit.Test;

public class EnumValueValidatorTest extends BaseValidatorTest {

    private enum DummyEnum {
        VALUE1, VALUE2, VALUE3
    }

    private enum EmptyEnum {
    }

    private class Entity {

        @EnumValue(enumClass = DummyEnum.class)
        private final String enumText;

        private Entity(final String enumText) {
            this.enumText = enumText;
        }

    }

    private class EntityWithEmptyEnum {

        @EnumValue(enumClass = EmptyEnum.class)
        private final String enumText;

        private EntityWithEmptyEnum(final String enumText) {
            this.enumText = enumText;
        }

    }

    @Test
    @SuppressWarnings("unchecked")
    public void testValidEnum() {
        final Entity entity = new Entity("VALUE1");

        assertNoViolation(validator.<Object>validate(entity));
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testInvalidEnum() {
        final Entity entity = new Entity("VALUEABC");

        assertViolations(validator.<Object>validate(entity));
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testInvalidEnum_ignoringCase() {
        final Entity entity = new Entity("valueabc");

        assertViolations(validator.<Object>validate(entity));
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testInvalidEnum_emptyEnum() {
        final EntityWithEmptyEnum entity = new EntityWithEmptyEnum("value");

        assertViolations(validator.<Object>validate(entity));
    }

}