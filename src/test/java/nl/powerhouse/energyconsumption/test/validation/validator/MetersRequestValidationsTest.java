package nl.powerhouse.energyconsumption.test.validation.validator;

import java.util.Arrays;

import nl.powerhouse.energyconsumption.model.Meter;
import nl.powerhouse.energyconsumption.model.MetersRequest;
import nl.powerhouse.energyconsumption.model.MonthType;
import nl.powerhouse.energyconsumption.test.validation.BaseValidatorTest;
import org.junit.Test;

public class MetersRequestValidationsTest extends BaseValidatorTest<MetersRequest> {

    @Test
    public void testMetersRequestValid() {
        final MetersRequest request = createValidMetersRequest();
        assertNoViolation(validator.validate(request));
    }

    @Test
    public void testMetersRequest_invalid_BlankId() {
        final MetersRequest request = new MetersRequest(Arrays.asList(new Meter("", MonthType.APR.name(), "A", 1)));
        assertViolations(validator.validate(request));
    }

    @Test
    public void testMetersRequest_invalid_BlankMonth() {
        final MetersRequest request = new MetersRequest(Arrays.asList(new Meter("1", "", "A", 1)));
        assertViolations(validator.validate(request));
    }

    @Test
    public void testMetersRequest_invalid_invalidMonth() {
        final MetersRequest request = new MetersRequest(Arrays.asList(new Meter("1", "JA", "A", 1)));
        assertViolations(validator.validate(request));
    }

    @Test
    public void testMetersRequest_invalid_BlankProfile() {
        final MetersRequest request = new MetersRequest(Arrays.asList(new Meter("1", MonthType.APR.name(), "", 1)));
        assertViolations(validator.validate(request));
    }

    @Test
    public void testMetersRequest_invalid_nullMeter() {
        final MetersRequest request = new MetersRequest(Arrays.asList(new Meter("1", MonthType.APR.name(), "A", null)));
        assertViolations(validator.validate(request));
    }

    private MetersRequest createValidMetersRequest() {
        return new MetersRequest(Arrays.asList(new Meter("1", MonthType.APR.name(), "A", 1)));
    }
}
