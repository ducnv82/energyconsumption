package nl.powerhouse.energyconsumption.test.validation.validator;


import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import nl.powerhouse.energyconsumption.model.MonthType;
import nl.powerhouse.energyconsumption.model.ProfileFraction;
import nl.powerhouse.energyconsumption.model.ProfilesFractionsRequest;
import nl.powerhouse.energyconsumption.test.validation.BaseValidatorTest;
import nl.powerhouse.energyconsumption.validation.constraints.OrderedChecks;
import org.junit.Test;

public class SumAllFractionsValidatorTest extends BaseValidatorTest<ProfilesFractionsRequest> {

    @Test
    public void testProfileFractionRequestValid() {
        final ProfilesFractionsRequest request = createValidProfileFractionRequest();
        assertNoViolation(validator.validate(request, OrderedChecks.class));
    }

    @Test
    public void testProfileFractionRequestInValid_blankMonth() {
        final ProfilesFractionsRequest request = new ProfilesFractionsRequest(Arrays.asList(new ProfileFraction("", "A", 1d)));
        assertViolations(validator.validate(request, OrderedChecks.class));
    }

    @Test
    public void testProfileFractionRequestInValid_blankProfile() {
        final ProfilesFractionsRequest request = new ProfilesFractionsRequest(Arrays.asList(new ProfileFraction("JAN", "", 1d)));
        assertViolations(validator.validate(request, OrderedChecks.class));
    }

    @Test
    public void testProfileFractionRequestInValid_blankFraction() {
        final ProfilesFractionsRequest request = new ProfilesFractionsRequest(Arrays.asList(new ProfileFraction("JAN", "A", null)));
        assertViolations(validator.validate(request, OrderedChecks.class));
    }

    @Test
    public void testProfileFractionRequestInValid_invalidMonth() {
        final ProfilesFractionsRequest request = new ProfilesFractionsRequest(Arrays.asList(new ProfileFraction("x", "A", 1d)));
        assertViolations(validator.validate(request, OrderedChecks.class));
    }

    @Test
    public void testProfileFractionRequestInValid_sumAllFractionsIn2ProfilesGreaterThan1() {
        final ProfilesFractionsRequest request = createinvalidProfileFractionRequest_sumAllFractionsIn2ProfilesGreaterThan1();
        assertViolations(validator.validate(request, OrderedChecks.class));
    }

    @Test
    public void testProfileFractionRequestInValid_sumAllFractionsIn2ProfilesSmallerThan1() {
        final ProfilesFractionsRequest request = createinvalidProfileFractionRequest_sumAllFractionsIn2ProfilesSmallerThan1();
        assertViolations(validator.validate(request, OrderedChecks.class));
    }

    @Test
    public void testProfileFractionRequestInValid_sumAllFractionsIn1ProfileSmallerThan1() {
        final ProfilesFractionsRequest request = createinvalidProfileFractionRequest_sumAllFractionsIn1ProfileSmallerThan1();
        assertViolations(validator.validate(request, OrderedChecks.class));
    }

    @Test
    public void testProfileFractionRequestInValid_sumAllFractionsIn1ProfileGreaterThan1() {
        final ProfilesFractionsRequest request = createinvalidProfileFractionRequest_sumAllFractionsIn1ProfileGreaterThan1();
        assertViolations(validator.validate(request, OrderedChecks.class));
    }

    private ProfilesFractionsRequest createValidProfileFractionRequest() {
        final List<ProfileFraction> list = Arrays.asList(
                new ProfileFraction(MonthType.JAN.toString(), "A", 0.01),
                new ProfileFraction(MonthType.FEB.toString(), "A", 0.01),
                new ProfileFraction(MonthType.MAR.toString(), "A", 0.01),
                new ProfileFraction(MonthType.APR.toString(), "A", 0.01),
                new ProfileFraction(MonthType.MAY.toString(), "A", 0.01),
                new ProfileFraction(MonthType.JUN.toString(), "A", 0.01),
                new ProfileFraction(MonthType.JUL.toString(), "A", 0.01),
                new ProfileFraction(MonthType.AUG.toString(), "A", 0.01),
                new ProfileFraction(MonthType.SEP.toString(), "A", 0.01),
                new ProfileFraction(MonthType.OCT.toString(), "A", 0.01),
                new ProfileFraction(MonthType.NOV.toString(), "A", 0.45),
                new ProfileFraction(MonthType.DEC.toString(), "A", 0.45),
                new ProfileFraction(MonthType.JAN.toString(), "B", 0.01),
                new ProfileFraction(MonthType.FEB.toString(), "B", 0.01),
                new ProfileFraction(MonthType.MAR.toString(), "B", 0.01),
                new ProfileFraction(MonthType.APR.toString(), "B", 0.01),
                new ProfileFraction(MonthType.MAY.toString(), "B", 0.01),
                new ProfileFraction(MonthType.JUN.toString(), "B", 0.01),
                new ProfileFraction(MonthType.JUL.toString(), "B", 0.01),
                new ProfileFraction(MonthType.AUG.toString(), "B", 0.01),
                new ProfileFraction(MonthType.SEP.toString(), "B", 0.01),
                new ProfileFraction(MonthType.OCT.toString(), "B", 0.01),
                new ProfileFraction(MonthType.NOV.toString(), "B", 0.45),
                new ProfileFraction(MonthType.DEC.toString(), "B", 0.45));

        Collections.shuffle(list, new Random(System.nanoTime()));

        return new ProfilesFractionsRequest(list);
    }

    private ProfilesFractionsRequest createinvalidProfileFractionRequest_sumAllFractionsIn2ProfilesGreaterThan1() {
        final List<ProfileFraction> list = Arrays.asList(
                new ProfileFraction(MonthType.JAN.toString(), "A", 0.01),
                new ProfileFraction(MonthType.FEB.toString(), "A", 0.01),
                new ProfileFraction(MonthType.MAR.toString(), "A", 0.01),
                new ProfileFraction(MonthType.APR.toString(), "A", 0.01),
                new ProfileFraction(MonthType.MAY.toString(), "A", 0.01),
                new ProfileFraction(MonthType.JUN.toString(), "A", 0.01),
                new ProfileFraction(MonthType.JUL.toString(), "A", 0.01),
                new ProfileFraction(MonthType.AUG.toString(), "A", 0.01),
                new ProfileFraction(MonthType.SEP.toString(), "A", 0.01),
                new ProfileFraction(MonthType.OCT.toString(), "A", 0.01),
                new ProfileFraction(MonthType.NOV.toString(), "A", 0.9),
                new ProfileFraction(MonthType.DEC.toString(), "A", 0.8),
                new ProfileFraction(MonthType.JAN.toString(), "B", 0.01),
                new ProfileFraction(MonthType.FEB.toString(), "B", 0.01),
                new ProfileFraction(MonthType.MAR.toString(), "B", 0.01),
                new ProfileFraction(MonthType.APR.toString(), "B", 0.01),
                new ProfileFraction(MonthType.MAY.toString(), "B", 0.01),
                new ProfileFraction(MonthType.JUN.toString(), "B", 0.01),
                new ProfileFraction(MonthType.JUL.toString(), "B", 0.01),
                new ProfileFraction(MonthType.AUG.toString(), "B", 0.01),
                new ProfileFraction(MonthType.SEP.toString(), "B", 0.01),
                new ProfileFraction(MonthType.OCT.toString(), "B", 0.01),
                new ProfileFraction(MonthType.NOV.toString(), "B", 0.2),
                new ProfileFraction(MonthType.DEC.toString(), "B", 0.6));

        Collections.shuffle(list, new Random(System.nanoTime()));

        return new ProfilesFractionsRequest(list);
    }

    private ProfilesFractionsRequest createinvalidProfileFractionRequest_sumAllFractionsIn2ProfilesSmallerThan1() {
        final List<ProfileFraction> list = Arrays.asList(
                new ProfileFraction(MonthType.JAN.toString(), "A", 0.01),
                new ProfileFraction(MonthType.FEB.toString(), "A", 0.01),
                new ProfileFraction(MonthType.MAR.toString(), "A", 0.01),
                new ProfileFraction(MonthType.APR.toString(), "A", 0.01),
                new ProfileFraction(MonthType.MAY.toString(), "A", 0.01),
                new ProfileFraction(MonthType.JUN.toString(), "A", 0.01),
                new ProfileFraction(MonthType.JUL.toString(), "A", 0.01),
                new ProfileFraction(MonthType.AUG.toString(), "A", 0.01),
                new ProfileFraction(MonthType.SEP.toString(), "A", 0.01),
                new ProfileFraction(MonthType.OCT.toString(), "A", 0.01),
                new ProfileFraction(MonthType.NOV.toString(), "A", 0.000001),
                new ProfileFraction(MonthType.DEC.toString(), "A", 0.000001),
                new ProfileFraction(MonthType.JAN.toString(), "B", 0.01),
                new ProfileFraction(MonthType.FEB.toString(), "B", 0.01),
                new ProfileFraction(MonthType.MAR.toString(), "B", 0.01),
                new ProfileFraction(MonthType.APR.toString(), "B", 0.01),
                new ProfileFraction(MonthType.MAY.toString(), "B", 0.01),
                new ProfileFraction(MonthType.JUN.toString(), "B", 0.01),
                new ProfileFraction(MonthType.JUL.toString(), "B", 0.01),
                new ProfileFraction(MonthType.AUG.toString(), "B", 0.01),
                new ProfileFraction(MonthType.SEP.toString(), "B", 0.01),
                new ProfileFraction(MonthType.OCT.toString(), "B", 0.01),
                new ProfileFraction(MonthType.NOV.toString(), "B", 0.000000000000002),
                new ProfileFraction(MonthType.DEC.toString(), "B", 0.000000000000006));

        Collections.shuffle(list, new Random(System.nanoTime()));

        return new ProfilesFractionsRequest(list);
    }

    private ProfilesFractionsRequest createinvalidProfileFractionRequest_sumAllFractionsIn1ProfileSmallerThan1() {
        final List<ProfileFraction> list = Arrays.asList(
                new ProfileFraction(MonthType.JAN.toString(), "A", 0.01),
                new ProfileFraction(MonthType.FEB.toString(), "A", 0.01),
                new ProfileFraction(MonthType.MAR.toString(), "A", 0.01),
                new ProfileFraction(MonthType.APR.toString(), "A", 0.01),
                new ProfileFraction(MonthType.MAY.toString(), "A", 0.01),
                new ProfileFraction(MonthType.JUN.toString(), "A", 0.01),
                new ProfileFraction(MonthType.JUL.toString(), "A", 0.01),
                new ProfileFraction(MonthType.AUG.toString(), "A", 0.01),
                new ProfileFraction(MonthType.SEP.toString(), "A", 0.01),
                new ProfileFraction(MonthType.OCT.toString(), "A", 0.01),
                new ProfileFraction(MonthType.NOV.toString(), "A", 0.0000001),
                new ProfileFraction(MonthType.DEC.toString(), "A", 0.0000001),
                new ProfileFraction(MonthType.JAN.toString(), "B", 0.01),
                new ProfileFraction(MonthType.FEB.toString(), "B", 0.01),
                new ProfileFraction(MonthType.MAR.toString(), "B", 0.01),
                new ProfileFraction(MonthType.APR.toString(), "B", 0.01),
                new ProfileFraction(MonthType.MAY.toString(), "B", 0.01),
                new ProfileFraction(MonthType.JUN.toString(), "B", 0.01),
                new ProfileFraction(MonthType.JUL.toString(), "B", 0.01),
                new ProfileFraction(MonthType.AUG.toString(), "B", 0.01),
                new ProfileFraction(MonthType.SEP.toString(), "B", 0.01),
                new ProfileFraction(MonthType.OCT.toString(), "B", 0.01),
                new ProfileFraction(MonthType.NOV.toString(), "B", 0.45),
                new ProfileFraction(MonthType.DEC.toString(), "B", 0.45));

        Collections.shuffle(list, new Random(System.nanoTime()));

        return new ProfilesFractionsRequest(list);
    }

    private ProfilesFractionsRequest createinvalidProfileFractionRequest_sumAllFractionsIn1ProfileGreaterThan1() {
        final List<ProfileFraction> list = Arrays.asList(
                new ProfileFraction(MonthType.JAN.toString(), "A", 0.01),
                new ProfileFraction(MonthType.FEB.toString(), "A", 0.01),
                new ProfileFraction(MonthType.MAR.toString(), "A", 0.01),
                new ProfileFraction(MonthType.APR.toString(), "A", 0.01),
                new ProfileFraction(MonthType.MAY.toString(), "A", 0.01),
                new ProfileFraction(MonthType.JUN.toString(), "A", 0.01),
                new ProfileFraction(MonthType.JUL.toString(), "A", 0.01),
                new ProfileFraction(MonthType.AUG.toString(), "A", 0.01),
                new ProfileFraction(MonthType.SEP.toString(), "A", 0.01),
                new ProfileFraction(MonthType.OCT.toString(), "A", 0.01),
                new ProfileFraction(MonthType.NOV.toString(), "A", 0.45),
                new ProfileFraction(MonthType.DEC.toString(), "A", 0.45),
                new ProfileFraction(MonthType.JAN.toString(), "B", 0.01),
                new ProfileFraction(MonthType.FEB.toString(), "B", 0.01),
                new ProfileFraction(MonthType.MAR.toString(), "B", 0.01),
                new ProfileFraction(MonthType.APR.toString(), "B", 0.01),
                new ProfileFraction(MonthType.MAY.toString(), "B", 0.01),
                new ProfileFraction(MonthType.JUN.toString(), "B", 0.01),
                new ProfileFraction(MonthType.JUL.toString(), "B", 0.01),
                new ProfileFraction(MonthType.AUG.toString(), "B", 0.01),
                new ProfileFraction(MonthType.SEP.toString(), "B", 0.01),
                new ProfileFraction(MonthType.OCT.toString(), "B", 0.01),
                new ProfileFraction(MonthType.NOV.toString(), "B", 0.8),
                new ProfileFraction(MonthType.DEC.toString(), "B", 0.9));

        Collections.shuffle(list, new Random(System.nanoTime()));

        return new ProfilesFractionsRequest(list);
    }
}
