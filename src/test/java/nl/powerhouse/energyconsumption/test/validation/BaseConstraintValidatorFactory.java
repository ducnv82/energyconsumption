package nl.powerhouse.energyconsumption.test.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorFactory;
import javax.validation.ValidationException;

/**
 * Base {@link javax.validation.ConstraintValidatorFactory} which is mainly used to inject instances to validator.
 *
 */
public abstract class BaseConstraintValidatorFactory implements ConstraintValidatorFactory {

    @Override
    public <T extends ConstraintValidator<?, ?>> T getInstance(final Class<T> key) {
        final T constraintValidator;

        try {
            constraintValidator = key.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            throw new ValidationException(e);
        }

        return constraintValidator;
    }

    @Override
    public void releaseInstance(final ConstraintValidator<?, ?> instance) {
    }

}
