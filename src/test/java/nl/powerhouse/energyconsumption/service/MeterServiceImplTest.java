package nl.powerhouse.energyconsumption.service;

import java.util.Arrays;
import java.util.List;

import mockit.Expectations;
import mockit.Injectable;
import mockit.Tested;
import mockit.Verifications;
import mockit.integration.junit4.JMockit;
import nl.powerhouse.energyconsumption.model.ConsumptionResponse;
import nl.powerhouse.energyconsumption.model.CreateMeterResponse;
import nl.powerhouse.energyconsumption.model.ErrorResponse;
import nl.powerhouse.energyconsumption.model.Meter;
import nl.powerhouse.energyconsumption.model.MonthType;
import nl.powerhouse.energyconsumption.model.ProfileFraction;
import nl.powerhouse.energyconsumption.repository.MeterRepository;
import nl.powerhouse.energyconsumption.repository.ProfileFractionMongoRepository;
import org.hamcrest.MatcherAssert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.http.ResponseEntity;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@RunWith(JMockit.class)
public class MeterServiceImplTest {

    @Tested
    private MeterServiceImpl meterService;

    @Injectable
    private MeterRepository meterRepository;

    @Injectable
    private ProfileFractionMongoRepository profileFractionMongoRepository;


    @Test
    public void testSaveMetersInOneYear_valid() {
        setUpExpectation_existingFraction();

        CreateMeterResponse response = meterService.saveMetersInOneYear(createValidMeters());

        new Verifications() {{
            meterRepository.save(response.getCreatedValidMeters());
            assertEquals(0, response.getInvaliddMeters().size());
        }};
    }

    @Test
    public void testSaveMetersInOneYear_invalid_notFoundFraction() {
        final List<Meter> meters = createValidMeters();
        Meter firstMeter = meters.get(0);

        new Expectations() {{
            profileFractionMongoRepository.countByMonthAndProfile(firstMeter.getMonth(), firstMeter.getProfile());
            result = 0;
        }};

        CreateMeterResponse response = meterService.saveMetersInOneYear(meters);

        new Verifications() {{
            assertEquals(meters.size() - response.getInvaliddMeters().size(), response.getCreatedValidMeters().size());
            assertEquals(meters.size() - response.getCreatedValidMeters().size(), response.getInvaliddMeters().size());
        }};
    }

    @Test
    public void testSaveMetersInOneYear_invalid_meterInLaterMonthLessThanPreviousMonthIn1Profile() {
        invalid_meterInLaterMonthLessThanPreviousMonth(false);
    }

    @Test
    public void testSaveMetersInOneYear_invalid_meterInLaterMonthLessThanPreviousMonthIn2Profiles() {
        invalid_meterInLaterMonthLessThanPreviousMonth(true);
    }

    @Test
    public void testSaveMetersInOneYear_invalid_meterInAllMonthsIn2Profiles() {
        setUpExpectation_existingFraction();

        final List<Meter> meters = createInvalid_metersInAllMonthsIn2Profiles();
        CreateMeterResponse response = meterService.saveMetersInOneYear(meters);

        new Verifications() {{
            meterRepository.save((Iterable) any);
            times = 0;

            assertEquals(meters.size() - response.getInvaliddMeters().size(), response.getCreatedValidMeters().size());
            assertEquals(meters.size() - response.getCreatedValidMeters().size(), response.getInvaliddMeters().size());
        }};
    }

    @Test
    public void testFindAllMeters() {
        meterService.findAllMeters();

        new Verifications() {{
            meterRepository.findAll();
        }};
    }

    @Test
    public void testDeleteMeter() {
        final String id = "123";

        meterService.deleteMeter(id);

        new Verifications() {{
            meterRepository.delete(id);
        }};
    }

    @Test
    public void testUpdateMeter() {
        final String id = "123";
        final Meter meter = new Meter("1", MonthType.JAN.toString(), "A", 5);

        new Expectations() {{
            meterRepository.exists(id);
            result = true;
        }};

        meterService.updateMeter(id, meter);

        new Verifications() {{
            final Meter actual;
            meterRepository.save(actual = withCapture());
            assertEquals(meter, actual);
        }};
    }

    @Test
    public void testUpdateMeter_meterCannotBeFound() {
        final String id = "123";
        final Meter meter = new Meter("1", MonthType.JAN.toString(), "A", 5);

        new Expectations() {{
            meterRepository.exists(id);
            result = false;
        }};

        final ResponseEntity<?> response = meterService.updateMeter(id, meter);

        assertTrue(response.getBody() instanceof ErrorResponse);
        assertEquals("Meter with id '" + id + "' cannot be found",
                     ((ErrorResponse) response.getBody()).getMessage());

        assertTrue(((ErrorResponse) response.getBody()).getTime() > 0);
    }

    @Test
    public void testFindConsumptionForAMeterAndAMonth() {
        final List<Meter> metersByProfileAWithTotalConsumption240 = createMetersByProfileAWithTotalConsumption240();
        final Meter meter = metersByProfileAWithTotalConsumption240.get(1);
        final String meterId = meter.getId();
        final String month = meter.getMonth();
        final String profile = meter.getProfile();
        final double fraction = 0.2;

        new Expectations() {{
            meterRepository.findOne(meterId);
            result = meter;

            profileFractionMongoRepository.findByMonthAndProfile(month, profile);
            result = new ProfileFraction(month, profile, fraction);

            meterRepository.findByProfile(profile);
            result = metersByProfileAWithTotalConsumption240;
        }};

        Object responseBody = meterService.findConsumptionForAMeterAndAMonth(meterId, meter.getMonth()).getBody();

        new Verifications() {{
            assertTrue(responseBody instanceof ConsumptionResponse);
            ConsumptionResponse consumptionResponse = (ConsumptionResponse) responseBody;
            assertThat(consumptionResponse.getMin(), equalTo(36.0));
            assertThat(consumptionResponse.getMax(), equalTo(60.0));
        }};
    }

    @Test
    public void testFindConsumptionForAMeterAndAMonth_invalidMeterIdAndMonth() {
        final String meterId = "nonExistence";

        new Expectations() {{
            meterRepository.findOne(meterId);
            result = null;
        }};

        Object responseBody = meterService.findConsumptionForAMeterAndAMonth(meterId, "AAA").getBody();

        new Verifications() {{
            assertTrue(responseBody instanceof ErrorResponse);
            ErrorResponse errorResponse = (ErrorResponse) responseBody;
            assertThat(errorResponse.getMessage(), equalTo("Invalid month 'AAA'. Meter with id 'nonExistence' cannot be found."));
            MatcherAssert.assertThat(errorResponse.getTime(), greaterThan(0l));
        }};
    }

    @Test
    public void testFindConsumptionForAMeterAndAMonth_invalidMeterId() {
        final String meterId = "nonExistence";

        new Expectations() {{
            meterRepository.findOne(meterId);
            result = null;
        }};

        Object responseBody = meterService.findConsumptionForAMeterAndAMonth(meterId, MonthType.JAN.name()).getBody();

        new Verifications() {{
            assertTrue(responseBody instanceof ErrorResponse);
            ErrorResponse errorResponse = (ErrorResponse) responseBody;
            assertThat(errorResponse.getMessage(), equalTo("Meter with id 'nonExistence' cannot be found."));
            MatcherAssert.assertThat(errorResponse.getTime(), greaterThan(0l));
        }};
    }

    @Test
    public void testFindConsumptionForAMeterAndAMonth_invalidMonth() {
        final String meterId = "1";

        new Expectations() {{
            meterRepository.findOne(meterId);
            result = new Meter();
        }};

        Object responseBody = meterService.findConsumptionForAMeterAndAMonth(meterId, "AAA").getBody();

        new Verifications() {{
            assertTrue(responseBody instanceof ErrorResponse);
            ErrorResponse errorResponse = (ErrorResponse) responseBody;
            assertThat(errorResponse.getMessage(), equalTo("Invalid month 'AAA'. "));
            MatcherAssert.assertThat(errorResponse.getTime(), greaterThan(0l));
        }};
    }

    private void invalid_meterInLaterMonthLessThanPreviousMonth(final boolean invalidIn2Profiles) {
        setUpExpectation_existingFraction();

        final List<Meter> meters =
                invalidIn2Profiles ? createInvalid_metersInLaterMonthLessThanPreviousMonthIn2Profile()
                        : createInvalid_metersInLaterMonthLessThanPreviousMonthIn1Profile();
        CreateMeterResponse response = meterService.saveMetersInOneYear(meters);

        new Verifications() {{
            List<Meter> actualMeters;
            meterRepository.save(actualMeters = withCapture());
            assertNotNull(actualMeters);

            assertEquals(meters.size() - response.getInvaliddMeters().size(), response.getCreatedValidMeters().size());
            assertEquals(meters.size() - response.getCreatedValidMeters().size(), response.getInvaliddMeters().size());
        }};
    }

    private void setUpExpectation_existingFraction() {
        new Expectations() {{
            profileFractionMongoRepository.countByMonthAndProfile(anyString, anyString);
            result = 1;
        }};
    }

    private List<Meter> createValidMeters() {
        return Arrays.asList(
                new Meter("1", MonthType.JAN.name(), "A", 1),
                new Meter("2", MonthType.FEB.name(), "A", 2),
                new Meter("3", MonthType.MAR.name(), "A", 3),
                new Meter("4", MonthType.APR.name(), "A", 4),
                new Meter("5", MonthType.MAY.name(), "A", 5),
                new Meter("6", MonthType.JUN.name(), "A", 6),
                new Meter("7", MonthType.JUL.name(), "A", 7),
                new Meter("8", MonthType.AUG.name(), "A", 8),
                new Meter("9", MonthType.SEP.name(), "A", 9),
                new Meter("10", MonthType.OCT.name(), "A", 10),
                new Meter("11", MonthType.NOV.name(), "A", 11),
                new Meter("12", MonthType.DEC.name(), "A", 12),
                new Meter("13", MonthType.JAN.name(), "B", 1),
                new Meter("14", MonthType.FEB.name(), "B", 2),
                new Meter("15", MonthType.MAR.name(), "B", 3),
                new Meter("16", MonthType.APR.name(), "B", 4),
                new Meter("17", MonthType.MAY.name(), "B", 5),
                new Meter("18", MonthType.JUN.name(), "B", 6),
                new Meter("19", MonthType.JUL.name(), "B", 7),
                new Meter("20", MonthType.AUG.name(), "B", 8),
                new Meter("21", MonthType.SEP.name(), "B", 9),
                new Meter("22", MonthType.OCT.name(), "B", 10),
                new Meter("23", MonthType.NOV.name(), "B", 11),
                new Meter("24", MonthType.DEC.name(), "B", 12));
    }

    private List<Meter> createInvalid_metersInLaterMonthLessThanPreviousMonthIn1Profile() {
        return Arrays.asList(
                new Meter("1", MonthType.JAN.name(), "A", 12),
                new Meter("2", MonthType.FEB.name(), "A", 11),
                new Meter("3", MonthType.MAR.name(), "A", 10),
                new Meter("4", MonthType.APR.name(), "A", 9),
                new Meter("5", MonthType.MAY.name(), "A", 8),
                new Meter("6", MonthType.JUN.name(), "A", 7),
                new Meter("7", MonthType.JUL.name(), "A", 6),
                new Meter("8", MonthType.AUG.name(), "A", 5),
                new Meter("9", MonthType.SEP.name(), "A", 4),
                new Meter("10", MonthType.OCT.name(), "A", 3),
                new Meter("11", MonthType.NOV.name(), "A", 2),
                new Meter("12", MonthType.DEC.name(), "A", 1),
                new Meter("13", MonthType.JAN.name(), "B", 1),
                new Meter("14", MonthType.FEB.name(), "B", 2),
                new Meter("15", MonthType.MAR.name(), "B", 3),
                new Meter("16", MonthType.APR.name(), "B", 4),
                new Meter("17", MonthType.MAY.name(), "B", 5),
                new Meter("18", MonthType.JUN.name(), "B", 6),
                new Meter("19", MonthType.JUL.name(), "B", 7),
                new Meter("20", MonthType.AUG.name(), "B", 8),
                new Meter("21", MonthType.SEP.name(), "B", 9),
                new Meter("22", MonthType.OCT.name(), "B", 10),
                new Meter("23", MonthType.NOV.name(), "B", 11),
                new Meter("24", MonthType.DEC.name(), "B", 12));
    }

    private List<Meter> createInvalid_metersInLaterMonthLessThanPreviousMonthIn2Profile() {
        return Arrays.asList(
                new Meter("1", MonthType.JAN.name(), "A", 12),
                new Meter("2", MonthType.FEB.name(), "A", 11),
                new Meter("3", MonthType.MAR.name(), "A", 10),
                new Meter("4", MonthType.APR.name(), "A", 9),
                new Meter("5", MonthType.MAY.name(), "A", 8),
                new Meter("6", MonthType.JUN.name(), "A", 7),
                new Meter("7", MonthType.JUL.name(), "A", 6),
                new Meter("8", MonthType.AUG.name(), "A", 5),
                new Meter("9", MonthType.SEP.name(), "A", 4),
                new Meter("10", MonthType.OCT.name(), "A", 3),
                new Meter("11", MonthType.NOV.name(), "A", 2),
                new Meter("12", MonthType.DEC.name(), "A", 1),
                new Meter("13", MonthType.JAN.name(), "B", 12),
                new Meter("14", MonthType.FEB.name(), "B", 11),
                new Meter("15", MonthType.MAR.name(), "B", 10),
                new Meter("16", MonthType.APR.name(), "B", 9),
                new Meter("17", MonthType.MAY.name(), "B", 8),
                new Meter("18", MonthType.JUN.name(), "B", 7),
                new Meter("19", MonthType.JUL.name(), "B", 6),
                new Meter("20", MonthType.AUG.name(), "B", 5),
                new Meter("21", MonthType.SEP.name(), "B", 4),
                new Meter("22", MonthType.OCT.name(), "B", 3),
                new Meter("23", MonthType.NOV.name(), "B", 11),
                new Meter("24", MonthType.DEC.name(), "B", 12));
    }

    private List<Meter> createInvalid_metersInAllMonthsIn2Profiles() {
        return Arrays.asList(
                new Meter("1", MonthType.JAN.name(), "A", 12),
                new Meter("2", MonthType.FEB.name(), "A", 11),
                new Meter("3", MonthType.MAR.name(), "A", 10),
                new Meter("4", MonthType.APR.name(), "A", 9),
                new Meter("5", MonthType.MAY.name(), "A", 8),
                new Meter("6", MonthType.JUN.name(), "A", 7),
                new Meter("7", MonthType.JUL.name(), "A", 6),
                new Meter("8", MonthType.AUG.name(), "A", 5),
                new Meter("9", MonthType.SEP.name(), "A", 4),
                new Meter("10", MonthType.OCT.name(), "A", 3),
                new Meter("11", MonthType.NOV.name(), "A", 2),
                new Meter("12", MonthType.DEC.name(), "A", 1),
                new Meter("13", MonthType.JAN.name(), "B", 12),
                new Meter("14", MonthType.FEB.name(), "B", 11),
                new Meter("15", MonthType.MAR.name(), "B", 10),
                new Meter("16", MonthType.APR.name(), "B", 9),
                new Meter("17", MonthType.MAY.name(), "B", 8),
                new Meter("18", MonthType.JUN.name(), "B", 7),
                new Meter("19", MonthType.JUL.name(), "B", 6),
                new Meter("20", MonthType.AUG.name(), "B", 5),
                new Meter("21", MonthType.SEP.name(), "B", 4),
                new Meter("22", MonthType.OCT.name(), "B", 3),
                new Meter("23", MonthType.NOV.name(), "B", 2),
                new Meter("24", MonthType.DEC.name(), "B", 1));
    }

    private List<Meter> createMetersByProfileAWithTotalConsumption240() {
        return Arrays.asList(
                new Meter("1", MonthType.JAN.name(), "B", 1),
                new Meter("2", MonthType.FEB.name(), "B", 2),           //1
                new Meter("3", MonthType.MAR.name(), "B", 3),           //1
                new Meter("4", MonthType.APR.name(), "B", 4),           //1
                new Meter("5", MonthType.MAY.name(), "B", 5),           //1
                new Meter("6", MonthType.JUN.name(), "B", 6),           //1
                new Meter("7", MonthType.JUL.name(), "B", 7),           //1
                new Meter("8", MonthType.AUG.name(), "B", 8),           //1
                new Meter("9", MonthType.SEP.name(), "B", 9),           //1
                new Meter("10", MonthType.OCT.name(), "B", 10),         //1
                new Meter("11", MonthType.NOV.name(), "B", 110),        //100
                new Meter("12", MonthType.DEC.name(), "B", 240));       //130
    }
}
