package nl.powerhouse.energyconsumption.service;

import java.util.ArrayList;
import java.util.List;

import mockit.Expectations;
import mockit.Injectable;
import mockit.Tested;
import mockit.Verifications;
import mockit.integration.junit4.JMockit;
import nl.powerhouse.energyconsumption.model.ErrorResponse;
import nl.powerhouse.energyconsumption.model.MonthType;
import nl.powerhouse.energyconsumption.model.ProfileFraction;
import nl.powerhouse.energyconsumption.repository.MeterRepository;
import nl.powerhouse.energyconsumption.repository.ProfileFractionMongoRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.http.ResponseEntity;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(JMockit.class)
public class ProfileFractionServiceImplTest {

    @Tested
    private ProfileFractionServiceImpl profileFractionService;

    @Injectable
    private ProfileFractionMongoRepository profileFractionMongoRepository;

    @Injectable
    private MeterRepository meterRepository;

    @Test
    public void testSaveProfilesFractionsInOneYear() {
        final List<ProfileFraction> profileFractions = new ArrayList<>();

        profileFractionService.saveProfilesFractionsInOneYear(profileFractions);

        new Verifications() {{
            profileFractionMongoRepository.save(profileFractions);
        }};
    }

    @Test
    public void testFindAllProfilesAndFractions() {
        profileFractionService.findAllProfilesFractions();

        new Verifications() {{
            profileFractionMongoRepository.findAll();
        }};
    }

    @Test
    public void testDeleteProfileFraction() {
        final String id = "123";

        profileFractionService.deleteProfileFraction(id);

        new Verifications() {{
            profileFractionMongoRepository.delete(id);
        }};
    }

    @Test
    public void testUpdateProfileFraction() {
        final String id = "123";
        final ProfileFraction profileFraction = new ProfileFraction(MonthType.JAN.toString(), "A", 0.5);

        new Expectations() {{
            profileFractionMongoRepository.exists(id);
            result = true;
        }};

        profileFractionService.updateProfileFraction(id, profileFraction);

        new Verifications() {{
            final ProfileFraction actual;
            profileFractionMongoRepository.save(actual = withCapture());
            assertEquals(profileFraction, actual);
        }};
    }

    @Test
    public void testUpdateProfileFraction_profileFractionCannotBeFound() {
        final String id = "123";
        final ProfileFraction profileFraction = new ProfileFraction(MonthType.JAN.toString(), "A", 0.5);

        new Expectations() {{
            profileFractionMongoRepository.exists(id);
            result = false;
        }};

        final ResponseEntity<?> response = profileFractionService.updateProfileFraction(id, profileFraction);

        assertTrue(response.getBody() instanceof ErrorResponse);
        assertEquals("ProfileFraction with id '" + id + "' cannot be found",
                     ((ErrorResponse) response.getBody()).getMessage());

        assertTrue(((ErrorResponse) response.getBody()).getTime() > 0);
    }
}
