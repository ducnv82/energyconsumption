package nl.powerhouse.energyconsumption.repository;

import nl.powerhouse.energyconsumption.model.ProfileFraction;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ProfileFractionMongoRepository extends MongoRepository<ProfileFraction, String> {

    long countByMonthAndProfile(String month, String profile);

    ProfileFraction findByMonthAndProfile(String month, String profile);
}
