package nl.powerhouse.energyconsumption.repository;

import java.util.List;

import nl.powerhouse.energyconsumption.model.Meter;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface MeterRepository extends MongoRepository<Meter, String> {

    List<Meter> findByProfile(String profile);
}
