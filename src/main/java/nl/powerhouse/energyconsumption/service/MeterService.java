package nl.powerhouse.energyconsumption.service;

import java.util.List;

import nl.powerhouse.energyconsumption.model.CreateMeterResponse;
import nl.powerhouse.energyconsumption.model.Meter;
import org.springframework.http.ResponseEntity;

public interface MeterService {
    CreateMeterResponse saveMetersInOneYear(List<Meter> meters);

    List<Meter> findAllMeters();

    void deleteMeter(String id);

    ResponseEntity<?> updateMeter(String id, Meter meter);

    ResponseEntity<?> findConsumptionForAMeterAndAMonth(final String meterId, final String month);
}
