/*
 * Copyright (c) Einsights Pte. Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package nl.powerhouse.energyconsumption.service;

import java.util.List;

import nl.powerhouse.energyconsumption.model.ProfileFraction;
import org.springframework.http.ResponseEntity;

public interface ProfileFractionService {

    void saveProfilesFractionsInOneYear(List<ProfileFraction> profilesFractions);

    List<ProfileFraction> findAllProfilesFractions();

    void deleteProfileFraction(String id);

    ResponseEntity<?> updateProfileFraction(String id, ProfileFraction profileFraction);
}
