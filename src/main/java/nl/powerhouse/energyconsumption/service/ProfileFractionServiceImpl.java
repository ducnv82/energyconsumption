package nl.powerhouse.energyconsumption.service;

import java.util.List;

import nl.powerhouse.energyconsumption.model.ErrorResponse;
import nl.powerhouse.energyconsumption.model.ProfileFraction;
import nl.powerhouse.energyconsumption.repository.ProfileFractionMongoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class ProfileFractionServiceImpl implements ProfileFractionService {

    @Autowired
    private ProfileFractionMongoRepository profileFractionMongoRepository;

    @Override
    public void saveProfilesFractionsInOneYear(final List<ProfileFraction> profilesFractions) {
        /** Insert non-duplicated records silently
        final List<ProfileFraction> addingProfileFractions = new ArrayList<>();

        profileFractions.forEach(pf -> {
            if (profileFractionMongoRepository.countByMonthAndProfile(pf.getMonth(), pf.getProfile()) == 0) {
                addingProfileFractions.add(pf);
            }
        });


        if (!addingProfileFractions.isEmpty()) {
            profileFractionMongoRepository.save(addingProfileFractions);
        }  **/

        /** Throws duplicated exception if encountered **/
        profileFractionMongoRepository.save(profilesFractions);
    }

    @Override
    public List<ProfileFraction> findAllProfilesFractions() {
        return profileFractionMongoRepository.findAll();
    }

    @Override
    public void deleteProfileFraction(final String id) {
        profileFractionMongoRepository.delete(id);
    }

    @Override
    public ResponseEntity<?> updateProfileFraction(final String id, final ProfileFraction profileFraction) {
        if (!profileFractionMongoRepository.exists(id)) {
            return ResponseEntity.badRequest()
                    .body(new ErrorResponse("ProfileFraction with id '" + id + "' cannot be found"));
        }

        return ResponseEntity.ok(profileFractionMongoRepository.save(profileFraction));
    }
}
