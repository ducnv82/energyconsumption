package nl.powerhouse.energyconsumption.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import nl.powerhouse.energyconsumption.model.ConsumptionResponse;
import nl.powerhouse.energyconsumption.model.CreateMeterResponse;
import nl.powerhouse.energyconsumption.model.ErrorResponse;
import nl.powerhouse.energyconsumption.model.Meter;
import nl.powerhouse.energyconsumption.model.MonthType;
import nl.powerhouse.energyconsumption.model.ProfileFraction;
import nl.powerhouse.energyconsumption.repository.MeterRepository;
import nl.powerhouse.energyconsumption.repository.ProfileFractionMongoRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;

@Service
public class MeterServiceImpl implements MeterService {

    private static final int ACCEPTABLE_TOLERANCE_PERCENT = 25;

    private static final Logger LOGGER = LoggerFactory.getLogger(MeterServiceImpl.class);

    @Autowired
    private MeterRepository meterRepository;

    @Autowired
    private ProfileFractionMongoRepository profileFractionMongoRepository;

    @Override
    public CreateMeterResponse saveMetersInOneYear(final List<Meter> meters) {
        final List<Meter> invaliddMeters = new ArrayList<>();
        final List<Meter> creatingMeters = new ArrayList<>();

        final Map<String, List<Meter>> profileMetersMap = meters.stream().collect(groupingBy(Meter::getProfile, toList()));
        for (Entry<String, List<Meter>> profileMetersMapEntry : profileMetersMap.entrySet()) {
            String profile = profileMetersMapEntry.getKey();
            LOGGER.debug("profile: {}", profile);

            List<Meter> metersSortedByMeterValue = profileMetersMapEntry.getValue().stream().sorted(
                                                                          comparing(Meter::getMeter)).collect(toList());
            int i = 1;
            for (Meter meter : metersSortedByMeterValue) {
                LOGGER.debug("meter: {}", meter);
                String month = meter.getMonth();

                if (profileFractionMongoRepository.countByMonthAndProfile(month, meter.getProfile()) == 0 ||
                    i == 1 && !MonthType.JAN.name().equalsIgnoreCase(month)  ||
                    i == 2 && !MonthType.FEB.name().equalsIgnoreCase(month)  ||
                    i == 3 && !MonthType.MAR.name().equalsIgnoreCase(month)  ||
                    i == 4 && !MonthType.APR.name().equalsIgnoreCase(month)  ||
                    i == 5 && !MonthType.MAY.name().equalsIgnoreCase(month)  ||
                    i == 6 && !MonthType.JUN.name().equalsIgnoreCase(month)  ||
                    i == 7 && !MonthType.JUL.name().equalsIgnoreCase(month)  ||
                    i == 8 && !MonthType.AUG.name().equalsIgnoreCase(month)  ||
                    i == 9 && !MonthType.SEP.name().equalsIgnoreCase(month)  ||
                    i == 10 && !MonthType.OCT.name().equalsIgnoreCase(month) ||
                    i == 11 && !MonthType.NOV.name().equalsIgnoreCase(month) ||
                    i == 12 && !MonthType.DEC.name().equalsIgnoreCase(month)) {

                    invaliddMeters.add(meter);
                } else {
                    creatingMeters.add(meter);
                }
                i++;
            }
        }

        if (!creatingMeters.isEmpty()) {
            meterRepository.save(creatingMeters);
        }

        return new CreateMeterResponse(creatingMeters, invaliddMeters);
    }

    @Override
    public List<Meter> findAllMeters() {
        return meterRepository.findAll();
    }

    @Override
    public void deleteMeter(final String id) {
        meterRepository.delete(id);
    }

    @Override
    public ResponseEntity<?> updateMeter(final String id, final Meter meter) {
        if (!meterRepository.exists(id)) {
            return ResponseEntity.badRequest()
                    .body(new ErrorResponse("Meter with id '" + id + "' cannot be found"));
        }

        return ResponseEntity.ok(meterRepository.save(meter));
    }

    /**
     * 240 * 0.2 = Total*Fraction = 48
     * 48 + 25% (of 48 = 12) = 60
     * 48 - 25% (of 48 = 12) = 36
     *
     * (A) Sum all consumption of a profile in 1 year.
     * (B) Multiply sum of all consumption with fraction.
     * Calculate percentage of (B)
     * Calculate min, calculate max.
     */
    @Override
    public ResponseEntity<?> findConsumptionForAMeterAndAMonth(final String meterId, final String month) {
        final StringBuilder errorMessageBuilder = new StringBuilder();
        if (MonthType.findByName(month) == null) {
            errorMessageBuilder.append("Invalid month '").append(month).append("'. ");
        }

        final Meter calculatingMeter = meterRepository.findOne(meterId);
        if (calculatingMeter == null) {
            errorMessageBuilder.append("Meter with id '" + meterId + "' cannot be found.");
        }

        if (errorMessageBuilder.length() > 0) {
            return ResponseEntity.badRequest()
                    .body(new ErrorResponse(errorMessageBuilder.toString()));
        }

        final String calculatingMeterProfile = calculatingMeter.getProfile();
        final ProfileFraction fractionByMonthAndProfile = profileFractionMongoRepository.findByMonthAndProfile(month,
                                                                                               calculatingMeterProfile);
        final List<Meter> metersByProfile = meterRepository.findByProfile(calculatingMeterProfile);
        int sumAllConsumptionByProfile = calculateSumAllConsumptionsByProfile(metersByProfile);

        final double totalConsumptionMultiplyFraction = sumAllConsumptionByProfile * fractionByMonthAndProfile.getFraction();
        final double toleranceValue = totalConsumptionMultiplyFraction * ACCEPTABLE_TOLERANCE_PERCENT / 100;

        final double min = totalConsumptionMultiplyFraction - toleranceValue;
        final double max = totalConsumptionMultiplyFraction + toleranceValue;
        return ResponseEntity.ok(new ConsumptionResponse(min, max));
    }

    private int calculateSumAllConsumptionsByProfile(final List<Meter> metersByProfile) {
        int consumptionByProfileThisMonth = 0;
        int meterByProfileThistMonth = 0;
        int meterByProfileLastMonth = 0;
        int sumAllConsumptionsByProfile = 0;

        for (Meter meterByProfile : metersByProfile) {
            meterByProfileThistMonth = meterByProfile.getMeter();
            consumptionByProfileThisMonth = meterByProfileThistMonth - meterByProfileLastMonth;
            meterByProfileLastMonth = meterByProfileThistMonth;

            sumAllConsumptionsByProfile += consumptionByProfileThisMonth;
        }

        return sumAllConsumptionsByProfile;
    }
}
