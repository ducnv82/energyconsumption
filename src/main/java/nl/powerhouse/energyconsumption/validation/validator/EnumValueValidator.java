package nl.powerhouse.energyconsumption.validation.validator;


import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import nl.powerhouse.energyconsumption.validation.constraints.EnumValue;
import org.hibernate.validator.constraintvalidation.HibernateConstraintValidatorContext;

/**
 * {@link javax.validation.ConstraintValidator} for validating enumeration values.
 *
 */
public class EnumValueValidator implements ConstraintValidator<EnumValue, String> {

    private static final String COMMA = ", ";

    private EnumValue constraintAnnotation;

    @Override
    public void initialize(final EnumValue constraintAnnotation) {
        this.constraintAnnotation = constraintAnnotation;
    }

    @Override
    public boolean isValid(final String value, final ConstraintValidatorContext context) {
        context.disableDefaultConstraintViolation();

        boolean valid = false;

        final Object [] enumValues = constraintAnnotation.enumClass().getEnumConstants();
        final StringBuilder allEnumValuesBuilder = new StringBuilder();

        if (enumValues.length > 0) {
            for (final Object enumValue : enumValues) {
                final String enumValueString = enumValue.toString();
                if (enumValueString.equals(value)) {
                    valid = true;
                }
                allEnumValuesBuilder.append(enumValueString).append(COMMA);
            }
            allEnumValuesBuilder.setLength(allEnumValuesBuilder.length() - 2);
        }

        if (!valid) {
            ((HibernateConstraintValidatorContext) context)
                    .addExpressionVariable("enumValues", allEnumValuesBuilder.toString())
                    .buildConstraintViolationWithTemplate(
                            "{nl.powerhouse.energyconsumption.validation.constraints.EnumValue.message}")
                    .addConstraintViolation();
        }

        return valid;
    }

}
