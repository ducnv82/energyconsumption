package nl.powerhouse.energyconsumption.validation.validator;

import java.util.HashMap;
import java.util.Map;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import nl.powerhouse.energyconsumption.model.ProfileFraction;
import nl.powerhouse.energyconsumption.model.ProfilesFractionsRequest;
import nl.powerhouse.energyconsumption.validation.constraints.SumAllFraction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.summingDouble;

public class SumAllFractionsValidator implements ConstraintValidator<SumAllFraction, ProfilesFractionsRequest> {

    private static final Logger LOGGER = LoggerFactory.getLogger(SumAllFractionsValidator.class);
    private static final String VALID_KEY = "valid";

    @Override
    public void initialize(final SumAllFraction constraintAnnotation) {
        // EMPTY
    }

    @Override
    public boolean isValid(final ProfilesFractionsRequest profilesFractionsRequest,
                           final ConstraintValidatorContext context) {

        final Map<String, Boolean> validMap = new HashMap<>(1);
        validMap.put(VALID_KEY, true);

        profilesFractionsRequest.getProfilesFractions().stream()
                .collect(groupingBy(ProfileFraction::getProfile, summingDouble(ProfileFraction::getFraction)))
                .forEach((profile, sumFraction) -> {
                    LOGGER.debug("Profile: {}\t Sum fractions: {}", profile, sumFraction);
                    if (sumFraction != 1) {
                        validMap.put(VALID_KEY, false);
                    }
                });

        return validMap.get(VALID_KEY);
    }
}
