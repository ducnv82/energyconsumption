package nl.powerhouse.energyconsumption.validation.constraints;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import nl.powerhouse.energyconsumption.validation.validator.EnumValueValidator;

/**
 * Constraint for validating enumeration.
 *
 */
@Documented
@Constraint(validatedBy = {EnumValueValidator.class})
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
public @interface EnumValue {

    /**
     * Validation message.
     */
    String message() default "{nl.powerhouse.energyconsumption.validation.constraints.EnumValue.message}";

    /**
     * Groups.
     */
    Class<?>[] groups() default {
    };

    /**
     * Payload.
     */
    Class<? extends Payload>[] payload() default {
    };

    /**
     * The enumeration class to validate against.
     */
    Class<? extends java.lang.Enum<?>> enumClass();

}
