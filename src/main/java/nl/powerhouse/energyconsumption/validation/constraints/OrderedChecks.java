package nl.powerhouse.energyconsumption.validation.constraints;

import javax.validation.GroupSequence;
import javax.validation.groups.Default;

@GroupSequence({Default.class, CustomChecks.class})
public interface OrderedChecks {
    //EMPTY
}
