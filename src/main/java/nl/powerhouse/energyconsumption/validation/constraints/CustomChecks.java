package nl.powerhouse.energyconsumption.validation.constraints;

/**
 * Group marker for checking table fields when create or update.
 *
 */
public interface CustomChecks {
    //EMPTY
}
