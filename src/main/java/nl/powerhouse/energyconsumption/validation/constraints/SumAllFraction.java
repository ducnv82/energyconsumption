package nl.powerhouse.energyconsumption.validation.constraints;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import nl.powerhouse.energyconsumption.validation.validator.SumAllFractionsValidator;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = SumAllFractionsValidator.class)
@Documented
public @interface SumAllFraction {
    /**
     * Validation message.
     */
    String message() default "{nl.powerhouse.energyconsumption.validation.sumAllFractions.message}";

    /**
     * Groups.
     */
    Class<?>[] groups() default {
    };

    /**
     * Payload.
     */
    Class<? extends Payload>[] payload() default {
    };
}
