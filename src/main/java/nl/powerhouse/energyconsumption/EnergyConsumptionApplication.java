package nl.powerhouse.energyconsumption;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EnergyConsumptionApplication {

    public static void main(final String[] args) {
        SpringApplication.run(EnergyConsumptionApplication.class, args);
    }
}
