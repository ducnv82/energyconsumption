package nl.powerhouse.energyconsumption.model;

public class ErrorResponse {

    private String message;
    private long time = System.currentTimeMillis();

    public ErrorResponse() {
        // For frameworks
    }

    /**
     * Convenient constructor.
     *
     * @param message error message
     */
    public ErrorResponse(final String message) {
        this.message = message;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(final String message) {
        this.message = message;
    }

    /**
     * @return the time
     */
    public long getTime() {
        return time;
    }

    /**
     * @param time the time to set
     */
    public void setTime(final long time) {
        this.time = time;
    }
}
