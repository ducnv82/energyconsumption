package nl.powerhouse.energyconsumption.model;

import javax.validation.constraints.NotNull;

import nl.powerhouse.energyconsumption.validation.constraints.EnumValue;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Version;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
@CompoundIndexes({@CompoundIndex(name = "Unique_Comnbination-month_profile", def = "{'month' : 1, 'profile' : 1}",
                                 unique = true)})
public class ProfileFraction {

    @Id
    private String id;

    @Version
    private Long version;

    @EnumValue(enumClass = MonthType.class)
    @NotBlank
    private String month;

    @NotBlank
    private String profile;

    @NotNull
    private Double fraction;

    public ProfileFraction() {
        // EMPTY
    }

    /**
     * Constructor.
     *
     * @param month month
     * @param profile profile
     * @param fraction fraction
     */
    public ProfileFraction(final String month, final String profile, final Double fraction) {
        this.month = month;
        this.profile = profile;
        this.fraction = fraction;
    }

    /**
     * @return the month
     */
    public String getMonth() {
        return month;
    }
    /**
     * @param month the month to set
     */
    public void setMonth(final String month) {
        this.month = month;
    }
    /**
     * @return the profile
     */
    public String getProfile() {
        return profile;
    }
    /**
     * @param profile the profile to set
     */
    public void setProfile(final String profile) {
        this.profile = profile;
    }
    /**
     * @return the fraction
     */
    public Double getFraction() {
        return fraction;
    }
    /**
     * @param fraction the fraction to set
     */
    public void setFraction(final Double fraction) {
        this.fraction = fraction;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(final String id) {
        this.id = id;
    }

    /**
     * @return the version
     */
    public Long getVersion() {
        return version;
    }

    /**
     * @param version the version to set
     */
    public void setVersion(final Long version) {
        this.version = version;
    }
}
