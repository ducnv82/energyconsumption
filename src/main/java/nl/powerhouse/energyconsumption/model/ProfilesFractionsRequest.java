package nl.powerhouse.energyconsumption.model;

import java.util.List;

import javax.validation.Valid;

import nl.powerhouse.energyconsumption.validation.constraints.CustomChecks;
import nl.powerhouse.energyconsumption.validation.constraints.SumAllFraction;
import org.hibernate.validator.constraints.NotEmpty;

@SumAllFraction(groups = CustomChecks.class)
public class ProfilesFractionsRequest {

    @NotEmpty
    @Valid
    private List<ProfileFraction> profilesFractions;

    public ProfilesFractionsRequest() {
        // For frameworks
    }

    /**
     * Constructor.
     *
     * @param profileFractions profileFractions
     */
    public ProfilesFractionsRequest(final List<ProfileFraction> profileFractions) {
        this.profilesFractions = profileFractions;
    }

    /**
     * @return the profileFractions
     */
    public List<ProfileFraction> getProfilesFractions() {
        return profilesFractions;
    }

    /**
     * @param profileFractions the profileFractions to set
     */
    public void setProfilesFractions(final List<ProfileFraction> profileFractions) {
        this.profilesFractions = profileFractions;
    }
}
