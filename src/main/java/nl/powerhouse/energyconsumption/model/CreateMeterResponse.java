package nl.powerhouse.energyconsumption.model;

import java.util.List;

public class CreateMeterResponse {

    private List<Meter> createdValidMeters;
    private List<Meter> invaliddMeters;

    public CreateMeterResponse() {
        // For frameworks
    }

    /**
     * Constructor.
     *
     * @param createdValidMeters createdValidMeters
     * @param invaliddMeters invaliddMeters
     */
    public CreateMeterResponse(final List<Meter> createdValidMeters, final List<Meter> invaliddMeters) {
        this.createdValidMeters = createdValidMeters;
        this.invaliddMeters = invaliddMeters;
    }

    /**
     * @return the createdValidMeters
     */
    public List<Meter> getCreatedValidMeters() {
        return createdValidMeters;
    }

    /**
     * @param createdValidMeters the createdValidMeters to set
     */

    public void setCreatedValidMeters(final List<Meter> createdValidMeters) {
        this.createdValidMeters = createdValidMeters;
    }

    /**
     * @return the invaliddMeters
     */
    public List<Meter> getInvaliddMeters() {
        return invaliddMeters;
    }

    /**
     * @param invaliddMeters the invaliddMeters to set
     */
    public void setInvaliddMeters(final List<Meter> invaliddMeters) {
        this.invaliddMeters = invaliddMeters;
    }
}
