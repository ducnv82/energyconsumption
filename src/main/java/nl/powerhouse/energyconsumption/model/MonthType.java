package nl.powerhouse.energyconsumption.model;

import org.springframework.util.StringUtils;

public enum MonthType {
    JAN, FEB, MAR, APR, MAY, JUN, JUL, AUG, SEP, OCT, NOV, DEC;

    public static MonthType findByName(final String name) {
        if (!StringUtils.hasText(name)) {
            return null;
        }

        for (MonthType month : MonthType.values()) {
            if (month.name().equals(name)) {
                return month;
            }
        }

        return null;
    }
}
