package nl.powerhouse.energyconsumption.model;

import java.util.List;

import javax.validation.Valid;

import org.hibernate.validator.constraints.NotEmpty;

public class MetersRequest {

    @NotEmpty
    @Valid
    private List<Meter> meters;

    public MetersRequest() {
        // For frameworks
    }

    /**
     * @param meters
     */
    public MetersRequest(final List<Meter> meters) {
        this.meters = meters;
    }

    /**
     * @return the meters
     */
    public List<Meter> getMeters() {
        return meters;
    }

    /**
     * @param meters the meters to set
     */
    public void setMeters(final List<Meter> meters) {
        this.meters = meters;
    }
}
