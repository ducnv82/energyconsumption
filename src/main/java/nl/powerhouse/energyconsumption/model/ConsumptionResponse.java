package nl.powerhouse.energyconsumption.model;

public class ConsumptionResponse {

    private double min;

    private double max;

    public ConsumptionResponse() {
        // For frameworks
    }

    public ConsumptionResponse(final double min, final double max) {
        this.min = min;
        this.max = max;
    }

    /**
     * @return the min
     */
    public double getMin() {
        return min;
    }

    /**
     * @param min the min to set
     */
    public void setMin(final double min) {
        this.min = min;
    }

    /**
     * @return the max
     */
    public double getMax() {
        return max;
    }

    /**
     * @param max the max to set
     */
    public void setMax(final double max) {
        this.max = max;
    }
}
