package nl.powerhouse.energyconsumption.controller;

import java.util.List;

import javax.validation.Valid;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import nl.powerhouse.energyconsumption.model.ConsumptionResponse;
import nl.powerhouse.energyconsumption.model.CreateMeterResponse;
import nl.powerhouse.energyconsumption.model.ErrorResponse;
import nl.powerhouse.energyconsumption.model.Meter;
import nl.powerhouse.energyconsumption.model.MetersRequest;
import nl.powerhouse.energyconsumption.service.MeterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("meters")
public class MeterRestController {

    @Autowired
    private MeterService meterService;

    @RequestMapping(value = "one-year", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public CreateMeterResponse createMetersInOneYear(final @RequestBody @Valid MetersRequest request) {

        return meterService.saveMetersInOneYear(request.getMeters());
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<Meter> findAllMeters() {
        return meterService.findAllMeters();
    }

    @RequestMapping(value = "{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteMeter(final @PathVariable String id) {
        meterService.deleteMeter(id);
    }

    @RequestMapping(value = "{id}", method = RequestMethod.PUT)
    @ApiOperation(value = "updateMeter", nickname = "updateMeter", response = Meter.class)
    @ApiResponses({@ApiResponse(code = 400, message = "Invalid input", response = ErrorResponse.class)})
    public ResponseEntity<?> updateMeter(final @PathVariable String id,
                                         final @RequestBody @Valid Meter meter) {

        return meterService.updateMeter(id, meter);
    }

    @RequestMapping(value = "{meterId}/{month}", method = RequestMethod.GET)
    @ApiOperation(value = "findConsumptionForAMeterAndAMonth", nickname = "findConsumptionForAMeterAndAMonth",
                                                               response = ConsumptionResponse.class)
    @ApiResponses({
        @ApiResponse(code = 400, message ="Invalid input", response = ErrorResponse.class)
    })
    public ResponseEntity<?> findConsumptionForAMeterAndAMonth(final @PathVariable String meterId,
                                                               final @PathVariable String month) {

        return meterService.findConsumptionForAMeterAndAMonth(meterId, month);
    }
}
