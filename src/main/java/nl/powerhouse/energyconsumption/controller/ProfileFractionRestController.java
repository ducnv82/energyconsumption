package nl.powerhouse.energyconsumption.controller;

import java.util.List;

import javax.validation.Valid;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import nl.powerhouse.energyconsumption.model.ErrorResponse;
import nl.powerhouse.energyconsumption.model.ProfileFraction;
import nl.powerhouse.energyconsumption.model.ProfilesFractionsRequest;
import nl.powerhouse.energyconsumption.service.ProfileFractionService;
import nl.powerhouse.energyconsumption.validation.constraints.OrderedChecks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("profiles-fractions")
public class ProfileFractionRestController {

    @Autowired
    private ProfileFractionService profileFractionService;

    @RequestMapping(value = "one-year", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public void createProfilesAndFractionsInOneYear(
                                  final @RequestBody @Validated(OrderedChecks.class) ProfilesFractionsRequest request) {

        profileFractionService.saveProfilesFractionsInOneYear(request.getProfilesFractions());
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<ProfileFraction> findAllProfilesAndFractions() {
        return profileFractionService.findAllProfilesFractions();
    }

    @RequestMapping(value = "{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteProfileFraction(final @PathVariable String id) {
        profileFractionService.deleteProfileFraction(id);
    }

    @RequestMapping(value = "{id}", method = RequestMethod.PUT)
    @ApiOperation(value = "updateProfileFraction", nickname = "updateProfileFraction", response = ProfileFraction.class)
    @ApiResponses({@ApiResponse(code = 400, message = "Invalid input", response = ErrorResponse.class)})
    public ResponseEntity<?> updateProfileFraction(final @PathVariable String id,
                                                   final @RequestBody @Valid ProfileFraction profileFraction) {

        return profileFractionService.updateProfileFraction(id, profileFraction);
    }
}
